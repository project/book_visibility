# Book Visibility

This module enhances the [book module](https://www.drupal.org/docs/8/core/modules/book/overview) by creating a per book block visibility configuration.

It uses a plugin which extends [ConditionPluginBase](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Condition%21ConditionPluginBase.php/class/ConditionPluginBase/9.2.x) to add a new "Book" tab on the [block configuration page](https://www.drupal.org/docs/contributed-modules/block-class/block-configuration).

Many thanks to Jaypan for providing [this guide](https://www.jaypan.com/tutorial/custom-drupal-block-visibility-plugins-and-condition-plugin-api) for extending the ConditionPluginBase which helped a lot.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/book_visibility).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/book_visibility).

## Table of contents

- Requirements
- Installation
- Configuration

## Requirements

This module requires the following modules:

- [Book](https://www.drupal.org/docs/8/core/modules/book/overview)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

[Block configuration page](https://www.drupal.org/docs/contributed-modules/block-class/block-configuration).
