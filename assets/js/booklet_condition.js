/**
 * @file
 * Sets up the summary for block visibility on vertical tabs of block forms.
 * Credit to Jaypan for providing the template.
 * See https://www.jaypan.com/tutorial/custom-drupal-block-visibility-plugins-and-condition-plugin-api.
 */

(function($, Drupal) {
  /**
   * Provide the summary information for the block settings vertical tabs.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the block settings summaries.
   */
  const conditionChecked = [];
  Drupal.behaviors.blockSettingsSummaryBook = {
    attach(context) {
      // Set the summary on the vertical tab.

      // Only do something if vertical tab exists.
      const element = $(
        '.vertical-tabs__menu-item-title:contains("Book")'
      ).next(".vertical-tabs__menu-link-summary");
      if (element.length > 0) {
        // Determine if the condition has been enabled (the box is checked).
        $(context)
          .find(
            'input[data-drupal-selector^="edit-visibility-book-book-visibility"][checked]'
          )
          .each(function() {
            conditionChecked.push(this);
          });

        if (conditionChecked.length) {
          // The condition has been enabled.
          return element.text(
            Drupal.t("The block is restricted to specific books.")
          );
        }
        // The condition has not been enabled.
        return element.text(Drupal.t("Not restricted"));
      }
    }
  };
})(jQuery, Drupal);
